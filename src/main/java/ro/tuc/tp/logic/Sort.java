package ro.tuc.tp.logic;

import ro.tuc.tp.dataModels.Task;
import java.util.Comparator;

public class Sort implements Comparator<Task> {
    public int compare(Task task1, Task task2)
    {
        return task1.getArrivalTime()-task2.getArrivalTime();
    }
}