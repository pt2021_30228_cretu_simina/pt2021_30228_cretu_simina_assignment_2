package ro.tuc.tp.logic;

import ro.tuc.tp.dataModels.Scheduler;
import ro.tuc.tp.dataModels.Task;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SimulationManager implements Runnable {
    public ArrayList<Task> generateServer;

    public int numberOfClients;
    public int numberOfQueues;
    public int timeSimulation;

    public int minArrivalTime;
    public int maxArrivalTime;
    public int minWaitingTime;
    public int maxWaitingTime;
    public int peakHour;

    private Scheduler scheduler;
    private java.io.File file;

    double averageWaitingTime;
    double averageServiceTime;

    public SimulationManager(java.io.File file1, java.io.File file2) {
        ArrayList<String> rez;
        rez = read(file1, "[1-9]{1}[0-9]*");
        this.numberOfClients = Integer.parseInt(rez.get(0));
        this.numberOfQueues = Integer.parseInt(rez.get(1));
        this.timeSimulation = Integer.parseInt(rez.get(2));
        this.minWaitingTime = Integer.parseInt(rez.get(3));
        this.maxWaitingTime = Integer.parseInt(rez.get(4));
        this.minArrivalTime = Integer.parseInt(rez.get(5));
        this.maxArrivalTime = Integer.parseInt(rez.get(6));
        this.file = file2;

        try {
            this.file.createNewFile();
        } catch (IOException ex) {
            System.out.println("The file doesn't exist");
        }
        createTask();
        scheduler = new Scheduler(numberOfQueues, numberOfClients);
    }

    public void run() {
        int customersServed = 0, maxWaitingTime = 0, time = 0;
        FileWriter rez;
        int x = 0;
        int maxClients = 0;
        int aux= 0;

        try {
            rez = new FileWriter(this.file.toString());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        while(true)
        {
            if (time >= timeSimulation || (generateServer.isEmpty() && maxWaitingTime <= 0)) break;
            while(!generateServer.isEmpty() && generateServer.get(0).getArrivalTime() == time)
            {
                scheduler.serve(generateServer.get(0));
                if (time + generateServer.get(0).getServiceTime() >= timeSimulation) {
                } else {
                    customersServed++;
                }
                generateServer.remove(0);
            }
            String string = getResult(time);
            try {
                rez.write(string);
            } catch (IOException ex) {
            }
            maxWaitingTime = maxWaitingTime(maxWaitingTime);
            time++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
        }

        for(int i = 0; i <generateServer.size(); i++){
            aux += generateServer.size();
            if(generateServer.size() > 0)
                x++;
        }
        if(maxClients < aux)
        {
            maxClients = aux;
            peakHour = time;
        }
        time++;

        try {
            rez.write("\nAverage service time: " + (averageServiceTime / customersServed));
            rez.write("\nAverage waiting time: " + (averageWaitingTime / customersServed));
            rez.write("\nPeak Hour : " + peakHour);
        } catch (IOException ex) {
        }
        try {
            rez.close();
        } catch (IOException ex) {
        }
    }

    private void createTask() {
        generateServer = new ArrayList<Task>(numberOfClients);
        int i = 0;
        while (i < numberOfClients) {
            Task task= new Task(getWaitingTime(), getProcessingTime(), i);
            generateServer.add(task);
            i++;
        }
        Collections.sort(generateServer, new Sort());
    }

    public int getProcessingTime() {
        int i = minArrivalTime + (int) (Math.random() * (maxArrivalTime - minArrivalTime));
        averageWaitingTime += i;
        return i;
    }

    public int getWaitingTime() {
        int i = minArrivalTime + (int) (Math.random() * (maxWaitingTime - minWaitingTime));
        averageServiceTime += i;
        return i;
    }

    private int maxWaitingTime(int maxWaitingTime) {
        if (!generateServer.isEmpty()) {
            maxWaitingTime--;
        } else {
            maxWaitingTime = scheduler.maxWaitingTime();
        }

        return maxWaitingTime;
    }

    public ArrayList<String> read(java.io.File file, String string) throws IllegalArgumentException {
        ArrayList<String> rez = new ArrayList<String>();
        try {
            Scanner scanner;
            try {
                scanner = new Scanner(file);
            } catch (FileNotFoundException ex) {
                throw new IllegalArgumentException("The file doesn't exist");
            }
            while(true) {
                if (!scanner.hasNextLine()) break;
                String nextLine = scanner.nextLine();
                String[] line = nextLine.split(",");

                for (int i = 0, splittedLineLength = line.length; i < splittedLineLength; i++) {
                    String str = line[i];
                    if (!str.matches("[1-9]{1}[0-9]*")) {
                        throw new IllegalArgumentException("Text introdus gresit");
                    } else {
                        rez.add(str);
                    }
                }
            }
            scanner.close();
        } catch(IllegalArgumentException ex) {
        }
        return rez;
    }

    private String getResult(int m) {
        String string = "\nTime: " + m + "\n";
        string = string + "Waiting clients:";
        for (int i = 0, generateServerSize = generateServer.size(); i < generateServerSize; i++) {
            Task task = generateServer.get(i);
            string = string + task.toString();
        }
        string = string + "\n" + scheduler.print();
        return string;
    }
}
