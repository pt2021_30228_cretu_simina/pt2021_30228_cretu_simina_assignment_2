package ro.tuc.tp.dataModels;

public class Task {

    private int id;
    private int arrivalTime;
    private int serviceTime;

    public Task(int arrivalTime, int serviceTimeie, int id) {
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTimeie;
        this.id =id;
    }
    public int getId(){
        return id;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime){
        this.arrivalTime = arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String toString()
    {
        return " ( " + id + ", " + arrivalTime + ", " + serviceTime + ") ";
    }

}
