package ro.tuc.tp.dataModels;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;

public class Server implements Runnable{
    private BlockingQueue<Task> tasks;
    private int waitingPeriod;

    private int idQueue;
    private boolean isRunning = true;

    public Server(int idQueue, int i) {
        this.idQueue = idQueue;
        this.tasks = new ArrayBlockingQueue<>(i);
    }

    public int getWaitingPeriod() {
        return waitingPeriod;
    }

    public int getIdQueue() {
        return idQueue;
    }

    @Override
    public void run() {
        while(true)
        {
            if (isRunning) {
                while (tasks.peek() != null) {
                    try {
                        int i = tasks.peek().getServiceTime();
                        Thread.sleep(1000);
                        i--;
                        waitingPeriod--;
                        tasks.peek().setServiceTime(i);
                        if (i != 0) {
                        } else {
                            tasks.peek().setServiceTime(0);
                            tasks.poll();
                        }
                    } catch (Exception ex) {
                    }
                }
                setRunning(false);
            } else {
                break;
            }
        }
    }

    public boolean isRunning() {
        return isRunning;
    }
    public void setRunning(boolean running) {
        this.isRunning = running;
    }

    public void addTask(Task newTask) {
        waitingPeriod += newTask.getServiceTime();
        tasks.add(newTask);
    }

    public String print()
    {
        String string = getWaitingPeriod() == 0 || tasks.peek().getServiceTime() == 0 || tasks.peek() == null ? "closed" : tasks.toString();
        return string;
    }
}
