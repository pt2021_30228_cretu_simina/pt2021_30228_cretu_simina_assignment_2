package ro.tuc.tp.dataModels;

import java.util.ArrayList;

public class Scheduler {
    private ArrayList<Server> servers;
    private ArrayList<Thread> threads;

    private int nrQueue;
    private int nrServers;

    public Scheduler(int nrQueue, int nrServers) {
        this.nrQueue = nrQueue;
        this.nrServers = nrServers;
        this.servers = new ArrayList<>(nrQueue);
        this.threads = new ArrayList<>(nrQueue);

        int i=0;
        while (i<nrQueue) {
            servers.add(new Server(i, nrQueue));
            threads.add(new Thread(servers.get(i)));
            threads.get(i).start();
            i++;
        }
    }

    public int getNrServers(){
        return nrServers;
    }

    public void serve(Task t) {
        servers.get(minWaitingTime()).addTask(t);
        if (servers.get(minWaitingTime()).isRunning() == true) {
            return;
        }
        servers.get(minWaitingTime()).setRunning(true);
        threads.set(minWaitingTime(), new Thread(servers.get(minWaitingTime())));
        threads.get(minWaitingTime()).start();
    }

    public int maxWaitingTime(){
        int maxtime = 0;
        for (int i = 0; i < servers.size(); i++) {
            Server server = servers.get(i);
            if (server.getWaitingPeriod() > maxtime) {
                maxtime = server.getWaitingPeriod();
            }
        }
        return maxtime;
    }

    private int minWaitingTime(){
        int waitingTime = 999;
        int q = 0;
        int i = 0;
        while (i < nrQueue) {
            if (servers.get(i).getWaitingPeriod() >= waitingTime) {
            } else {
                waitingTime = servers.get(i).getWaitingPeriod();
                q = i;
            }
            if (waitingTime == 0){
                break;
            }
            i++;
        }
        return q;
    }

    public String print() {
        String string="";
        for (int i = 0; i < servers.size(); i++) {
            Server server = servers.get(i);
            string = string + ("Queue " + server.getIdQueue() + " " + server.print() + "\n");
        }
        return string;
    }
}
