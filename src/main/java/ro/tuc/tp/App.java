package ro.tuc.tp;

import ro.tuc.tp.logic.SimulationManager;

import java.io.File;

public class App {

    public static void main(String args[]) throws Exception {
        SimulationManager simulationManager = new SimulationManager(new File("input.txt"), new File("output.txt"));
        Thread thread = new Thread(simulationManager);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }
}